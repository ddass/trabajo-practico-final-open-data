# Trabajo final de open data
El trabajo consiste en hacer una idea de negocios usando datos abiertos.

## Diccionario de datos
- Dataset Segmentacion Mercado. 
Este dataset se creo a partir del dataset [CUADRO 2.1.2. PROYECCIÓN DE LA POBLACIÓN TOTAL DEL PAÍS POR ÁREA, SEGÚN GRUPOS DE EDAD. AÑO 2019](https://www.datos.gov.py/dataset/anuario-estad%C3%ADstico-2019-poblaci%C3%B3n-y-vivienda/resource/d751e88e-5160-4d39-a3eb-b712abec9968#%7B%7D)
    - GRUPOS DE EDADES: Grupos de edades
    - AREA TOTAL: Total de personas por grupo de edad
    - AREA URBANA: Total de personas en zona urbana por edad
    - AREA RURAL: Total de personas en zona rural por edad
    - PORCENTAJE DE AREA URBANO: AREA URBANA/AREA TOTAL
    - PORCENTAJE DE AREA RURAL: AREA RURAL/AREA TOTAL
    - PORCENTAJE DE AREA URBANO DEL TOTAL: AREA URBANA/ total de personas en zona urbano
    - PORCENTAJE DE AREA RURAL DEL TOTAL: AREA RURAL/ total de personas en zona rural
- Dataset Competencia Energizante/Yerba Mate.
Los dataset comparten los nombres de las columnas
    - Marca: Marca del producto
    - Precio: Precio de venta a la hora de hacer el scraping

